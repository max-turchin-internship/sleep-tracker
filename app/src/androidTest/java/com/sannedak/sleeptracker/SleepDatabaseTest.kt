package com.sannedak.sleeptracker

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.sannedak.sleeptracker.data.model.SleepNight
import com.sannedak.sleeptracker.data.repository.SleepDao
import com.sannedak.sleeptracker.data.repository.SleepDatabase
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.lang.Exception

@RunWith(AndroidJUnit4::class)
class SleepDatabaseTest {

    private lateinit var sleepDao: SleepDao
    private lateinit var database: SleepDatabase

    @Before
    fun createDatabase() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        database = Room.inMemoryDatabaseBuilder(context, SleepDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        sleepDao = database.sleepDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDatabase() {
        database.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetNight() {
        runBlocking {
            val night = SleepNight()
            sleepDao.insert(night)
            val tonight = sleepDao.getTonight()
            assertEquals(tonight?.sleepQuality, -1)
        }
    }
}