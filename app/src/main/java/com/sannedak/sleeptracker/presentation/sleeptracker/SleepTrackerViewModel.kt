package com.sannedak.sleeptracker.presentation.sleeptracker

import androidx.lifecycle.*
import com.sannedak.sleeptracker.data.model.SleepNight
import com.sannedak.sleeptracker.data.repository.SleepRepository
import com.sannedak.sleeptracker.presentation.UiState
import com.sannedak.sleeptracker.presentation.sleeptracker.model.SleepNightMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect

import kotlinx.coroutines.launch

class SleepTrackerViewModel(
    private val repository: SleepRepository,
    private val mapper: SleepNightMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<UiState<Any>>(UiState.Idle)

    val uiState: StateFlow<UiState<Any>>
        get() = _uiState

    init {
        viewModelScope.launch {
            repository.getAllNightsRealtime().collect {
                _uiState.value = it
                when(it) {
                    is UiState.Loading -> _uiState.value = it
                    is UiState.FlowFromRepository -> convertToUi(it.data)
                }
            }
        }
        checkTrackingState()
    }

    private fun convertToUi(dataFlow: Flow<List<SleepNight>>) {
        viewModelScope.launch {
            dataFlow.collect {
                val mappedList = mapper.mapToUiNight(it)
                _uiState.value = UiState.ListContent(mappedList)
            }
        }
    }

    fun onStartTracking() = viewModelScope.launch {
        val newNight = SleepNight()
        insert(newNight)
        _uiState.value = UiState.TrackingOnProcess
    }

    fun onStopTracking() = viewModelScope.launch {
        val oldNight = getTonightFromDatabase() ?: return@launch
        val mappedNight = mapper.mapToStoppedTracking(oldNight)
        repository.updateInDatabase(mappedNight)
        _uiState.value = UiState.EndedTracking(mappedNight.nightId)
    }

    fun onClear() = viewModelScope.launch {
        repository.clearTableInDatabase()
        _uiState.value = UiState.ShowSnackBar
    }

    private fun checkTrackingState() = viewModelScope.launch {
        val tonight = getTonightFromDatabase()
        if (tonight?.nightId != null) {
            _uiState.value = UiState.TrackingOnProcess
        }
    }

    private suspend fun getTonightFromDatabase(): SleepNight? {
        var night = repository.getTonight()
        if (night?.startTimeMilli != night?.endTimeMilli) night = null
        return night
    }

    private fun insert(night: SleepNight) = viewModelScope.launch {
        repository.insertToDatabase(night)
    }
}
