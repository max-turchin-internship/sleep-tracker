package com.sannedak.sleeptracker.presentation.sleepquality

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sannedak.sleeptracker.SleepTrackerApplication
import com.sannedak.sleeptracker.databinding.FragmentSleepQualityBinding
import com.sannedak.sleeptracker.presentation.UiState
import kotlinx.coroutines.flow.collect

class SleepQualityFragment : Fragment() {

    private var _binding: FragmentSleepQualityBinding? = null

    private val args: SleepQualityFragmentArgs by navArgs()
    private val application by lazy { requireNotNull(this.activity).application }
    private val viewModel: SleepQualityViewModel by viewModels {
        SleepQualityViewModelFactory(
            args.nightId,
            (application as SleepTrackerApplication).repository,
            SleepTrackerApplication().sleepQualityMapper
        )
    }

    private val binding
        get() = _binding!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSleepQualityBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEventListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect { uiState ->
                when(uiState) {
                    is UiState.TransitionToAnotherScreen -> navigateToTrackerScreen()
                }
            }
        }
    }

    private fun navigateToTrackerScreen() {
        val action = SleepQualityFragmentDirections
            .actionSleepQualityFragmentToSleepTrackerFragment()
        this.findNavController().navigate(action)
    }

    private fun initEventListeners() {
        binding.imageQualityZero.setOnClickListener { viewModel.onSetSleepQuality(0) }
        binding.imageQualityOne.setOnClickListener { viewModel.onSetSleepQuality(1) }
        binding.imageQualityTwo.setOnClickListener { viewModel.onSetSleepQuality(2) }
        binding.imageQualityThree.setOnClickListener { viewModel.onSetSleepQuality(3) }
        binding.imageQualityFour.setOnClickListener { viewModel.onSetSleepQuality(4) }
        binding.imageQualityFive.setOnClickListener { viewModel.onSetSleepQuality(5) }
    }
}
