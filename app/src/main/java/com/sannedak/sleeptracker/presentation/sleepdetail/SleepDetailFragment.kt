package com.sannedak.sleeptracker.presentation.sleepdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sannedak.sleeptracker.SleepTrackerApplication
import com.sannedak.sleeptracker.databinding.FragmentSleepDetailBinding
import com.sannedak.sleeptracker.presentation.UiState
import com.sannedak.sleeptracker.presentation.sleepdetail.model.UiSleepNightDetail
import kotlinx.coroutines.flow.collect

class SleepDetailFragment : Fragment() {

    private var _binding: FragmentSleepDetailBinding? = null

    private val args: SleepDetailFragmentArgs by navArgs()
    private val application by lazy { requireNotNull(this.activity).application }
    private val viewModel: SleepDetailViewModel by viewModels {
        SleepDetailViewModelFactory(
            args.nightId,
            (application as SleepTrackerApplication).repository,
            SleepTrackerApplication().sleepDetailMapper
        )
    }

    private val binding
        get() = _binding!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSleepDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEventListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect { uiState ->
                when (uiState) {
                    is UiState.DetailContent -> showDetails(uiState.data)
                }
            }
        }
    }

    private fun initEventListeners() {
        binding.buttonClose.setOnClickListener { transitToListScreen() }
    }

    private fun showDetails(data: UiSleepNightDetail) {
        binding.imageSleepQuality.setImageResource(data.imageResource)
        binding.textSleepQuality.text = data.sleepQuality
        binding.textSleepTime.text = data.sleepTime
    }

    private fun transitToListScreen() {
        val action = SleepDetailFragmentDirections
            .actionSleepDetailFragmentToSleepTrackerFragment()
        this.findNavController().navigate(action)
    }
}