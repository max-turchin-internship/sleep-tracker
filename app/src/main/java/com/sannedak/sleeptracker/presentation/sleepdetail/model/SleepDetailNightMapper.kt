package com.sannedak.sleeptracker.presentation.sleepdetail.model

import android.content.res.Resources
import com.sannedak.sleeptracker.R
import com.sannedak.sleeptracker.data.model.SleepNight
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class SleepDetailNightMapper(private val resources: Resources) {

    private val ONE_MINUTE_MILLIS = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES)
    private val ONE_HOUR_MILLIS = TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS)

    fun mapToUiObject(night: SleepNight) = UiSleepNightDetail(
        convertNumericQualityToString(night.sleepQuality),
        convertNumericQualityToImage(night.sleepQuality),
        convertDurationToFormatted(night.startTimeMilli, night.endTimeMilli)
    )

    private fun convertNumericQualityToString(sleepQuality: Int) = when (sleepQuality) {
        0 -> resources.getString(R.string.zero_very_bad)
        1 -> resources.getString(R.string.one_poor)
        2 -> resources.getString(R.string.two_soso)
        3 -> resources.getString(R.string.three_ok)
        4 -> resources.getString(R.string.four_pretty_good)
        5 -> resources.getString(R.string.five_excellent)
        else -> "--"
    }

    private fun convertNumericQualityToImage(sleepQuality: Int) = when (sleepQuality) {
        0 -> R.drawable.ic_sleep_0
        1 -> R.drawable.ic_sleep_1
        2 -> R.drawable.ic_sleep_2
        3 -> R.drawable.ic_sleep_3
        4 -> R.drawable.ic_sleep_4
        5 -> R.drawable.ic_sleep_5
        else -> R.drawable.ic_sleep_active
    }

    private fun convertDurationToFormatted(
        startTimeMilli: Long,
        endTimeMilli: Long
    ): String {
        val durationMilli = endTimeMilli - startTimeMilli
        val weekdayString = SimpleDateFormat("EEEE", Locale.getDefault()).format(startTimeMilli)
        return when {
            durationMilli < ONE_MINUTE_MILLIS -> {
                val seconds = TimeUnit.SECONDS.convert(durationMilli, TimeUnit.MILLISECONDS)
                resources.getString(R.string.seconds_length, seconds, weekdayString)
            }
            durationMilli < ONE_HOUR_MILLIS -> {
                val minutes = TimeUnit.MINUTES.convert(durationMilli, TimeUnit.MILLISECONDS)
                resources.getString(R.string.minutes_length, minutes, weekdayString)
            }
            else -> {
                val hours = TimeUnit.HOURS.convert(durationMilli, TimeUnit.MILLISECONDS)
                resources.getString(R.string.hours_length, hours, weekdayString)
            }
        }
    }
}