package com.sannedak.sleeptracker.presentation.sleeptracker.model

import android.content.res.Resources
import com.sannedak.sleeptracker.R
import com.sannedak.sleeptracker.data.model.SleepNight
import java.lang.StringBuilder

class SleepNightMapper(private val resources: Resources) {

    fun mapToStoppedTracking(night: SleepNight) =
        SleepNight(
            night.nightId,
            night.startTimeMilli,
            System.currentTimeMillis(),
            night.sleepQuality
        )

    fun mapToUiNight(nightsList: List<SleepNight>) = nightsList.map { mapSleepNight(it) }

    private fun mapSleepNight(night: SleepNight) = UiSleepNight(
        night.nightId,
        convertNumericQualityToString(night.sleepQuality),
        convertNumericQualityToImage(night.sleepQuality)
    )

    private fun convertNumericQualityToString(sleepQuality: Int) = when (sleepQuality) {
        0 -> resources.getString(R.string.zero_very_bad)
        1 -> resources.getString(R.string.one_poor)
        2 -> resources.getString(R.string.two_soso)
        3 -> resources.getString(R.string.three_ok)
        4 -> resources.getString(R.string.four_pretty_good)
        5 -> resources.getString(R.string.five_excellent)
        else -> "--"
    }

    private fun convertNumericQualityToImage(sleepQuality: Int) = when (sleepQuality) {
        0 -> R.drawable.ic_sleep_0
        1 -> R.drawable.ic_sleep_1
        2 -> R.drawable.ic_sleep_2
        3 -> R.drawable.ic_sleep_3
        4 -> R.drawable.ic_sleep_4
        5 -> R.drawable.ic_sleep_5
        else -> R.drawable.ic_sleep_active
    }
}