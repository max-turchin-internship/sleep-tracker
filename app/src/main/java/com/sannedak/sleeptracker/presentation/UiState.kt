package com.sannedak.sleeptracker.presentation

import com.sannedak.sleeptracker.data.model.SleepNight
import com.sannedak.sleeptracker.presentation.sleepdetail.model.UiSleepNightDetail
import com.sannedak.sleeptracker.presentation.sleeptracker.model.UiSleepNight
import kotlinx.coroutines.flow.Flow

sealed class UiState<T> {

    object Idle: UiState<Any>()
    object Loading: UiState<Any>()
    object ShowSnackBar: UiState<Any>()
    object TrackingOnProcess: UiState<Any>()
    object TransitionToAnotherScreen: UiState<Any>()
    data class EndedTracking<T>(val nightId: Int): UiState<T>()
    data class FlowFromRepository<T>(val data: Flow<List<SleepNight>>): UiState<T>()
    data class ListContent<T>(val data: List<UiSleepNight>): UiState<T>()
    data class DetailContent<T>(val data: UiSleepNightDetail): UiState<T>()
}
