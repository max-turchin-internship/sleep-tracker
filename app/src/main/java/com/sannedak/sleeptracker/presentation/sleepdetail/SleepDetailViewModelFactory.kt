package com.sannedak.sleeptracker.presentation.sleepdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sannedak.sleeptracker.data.repository.SleepRepository
import com.sannedak.sleeptracker.presentation.sleepdetail.model.SleepDetailNightMapper
import java.lang.IllegalArgumentException

class SleepDetailViewModelFactory(
    private val nightId: Int,
    private val repository: SleepRepository,
    private val mapper: SleepDetailNightMapper
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SleepDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SleepDetailViewModel(nightId, repository, mapper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}