package com.sannedak.sleeptracker.presentation.sleepquality

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sannedak.sleeptracker.data.repository.SleepRepository
import com.sannedak.sleeptracker.presentation.sleepquality.model.SleepQualityMapper

class SleepQualityViewModelFactory(
    private val nightId: Int,
    private val repository: SleepRepository,
    private val mapper: SleepQualityMapper
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SleepQualityViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SleepQualityViewModel(nightId, repository, mapper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
