package com.sannedak.sleeptracker.presentation.sleeptracker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.sannedak.sleeptracker.R
import com.sannedak.sleeptracker.SleepTrackerApplication
import com.sannedak.sleeptracker.databinding.FragmentSleepTrackerBinding
import com.sannedak.sleeptracker.presentation.UiState
import com.sannedak.sleeptracker.presentation.sleeptracker.model.UiSleepNight
import kotlinx.coroutines.flow.collect

class SleepTrackerFragment : Fragment() {

    private var _binding: FragmentSleepTrackerBinding? = null

    private val application by lazy { requireNotNull(this.activity).application }
    private val listAdapter by lazy { SleepTrackerListAdapter(::showSleepDetails) }
    private val viewModel: SleepTrackerViewModel by viewModels {
        SleepTrackerViewModelFactory(
            (application as SleepTrackerApplication).repository,
            SleepTrackerApplication().sleepNightMapper
        )
    }

    private val binding
        get() = _binding!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSleepTrackerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecycler()
        initEventListeners()
    }

    private fun setUpRecycler() {
        val manager = GridLayoutManager(activity, 3)
        binding.listSleep.layoutManager = manager
        binding.listSleep.adapter = listAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect { uiState ->
                when (uiState) {
                    is UiState.ShowSnackBar -> showSnackBar()
                    is UiState.TrackingOnProcess -> onTracking()
                    is UiState.ListContent -> showList(uiState.data)
                    is UiState.EndedTracking -> navigateToSleepQuality(uiState.nightId)
                }
            }
        }
    }

    private fun navigateToSleepQuality(nightId: Int) {
        onStopTracking()
        val action = SleepTrackerFragmentDirections
            .actionSleepTrackerFragmentToSleepQualityFragment(nightId)
        this.findNavController().navigate(action)
    }

    private fun showSnackBar() {
        Snackbar.make(binding.root, getString(R.string.cleared_message), Snackbar.LENGTH_SHORT)
            .show()
    }

    private fun showList(list: List<UiSleepNight>) {
        listAdapter.refillList(list)
        listAdapter.submitList(list)
        if (list.isEmpty()) changeClearButtonState()
    }

    private fun onTracking() {
        binding.buttonStart.isEnabled = false
        binding.buttonStop.isEnabled = true
        binding.buttonClear.isEnabled = false
    }

    private fun onStopTracking() {
        binding.buttonStart.isEnabled = true
        binding.buttonStop.isEnabled = false
    }

    private fun changeClearButtonState() {
        binding.buttonClear.isEnabled = !binding.buttonClear.isEnabled
    }

    private fun initEventListeners() {
        binding.buttonStart.setOnClickListener { viewModel.onStartTracking() }
        binding.buttonStop.setOnClickListener { viewModel.onStopTracking() }
        binding.buttonClear.setOnClickListener { viewModel.onClear() }
    }

    private fun showSleepDetails(nightId: Int) {
        val action = SleepTrackerFragmentDirections
            .actionSleepTrackerFragmentToSleepDetailFragment(nightId)
        this.findNavController().navigate(action)
    }

}
