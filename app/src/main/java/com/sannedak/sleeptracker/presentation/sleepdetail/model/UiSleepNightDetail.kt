package com.sannedak.sleeptracker.presentation.sleepdetail.model

data class UiSleepNightDetail(

    val sleepQuality: String,
    val imageResource: Int,
    val sleepTime: String
)
