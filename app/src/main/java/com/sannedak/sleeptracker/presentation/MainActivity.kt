package com.sannedak.sleeptracker.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sannedak.sleeptracker.SleepTrackerApplication
import com.sannedak.sleeptracker.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        application as SleepTrackerApplication
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }
}
