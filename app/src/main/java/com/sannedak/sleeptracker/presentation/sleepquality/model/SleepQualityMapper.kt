package com.sannedak.sleeptracker.presentation.sleepquality.model

import com.sannedak.sleeptracker.data.model.SleepNight

class SleepQualityMapper {

    fun mapQuality(night: SleepNight, sleepQuality: Int) =
        SleepNight(night.nightId, night.startTimeMilli, night.endTimeMilli, sleepQuality)
}