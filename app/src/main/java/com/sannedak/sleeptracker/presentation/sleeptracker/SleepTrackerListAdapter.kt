package com.sannedak.sleeptracker.presentation.sleeptracker

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sannedak.sleeptracker.R
import com.sannedak.sleeptracker.databinding.ListItemSleepTrackerBinding
import com.sannedak.sleeptracker.presentation.sleeptracker.model.UiSleepNight

class SleepTrackerListAdapter(private val showSleepDetails: (nightId: Int) -> Unit) :
    ListAdapter<UiSleepNight, SleepListItemViewHolder>(SleepNightDiffCallback()) {

    init {
        setHasStableIds(true)
    }

    private val list = mutableListOf<UiSleepNight>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SleepListItemViewHolder {
        return SleepListItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_sleep_tracker,
                parent,
                false
            )
        )
    }

    override fun getItemId(position: Int): Long {
        val positionInRecycler = list[position].nightId
        return positionInRecycler.toLong()
    }

    override fun onBindViewHolder(holder: SleepListItemViewHolder, position: Int) {
        holder.bind(list[position], showSleepDetails)
    }

    override fun getItemCount() = list.size

    fun refillList(newList: List<UiSleepNight>) {
        list.clear()
        list.addAll(newList)
    }
}

class SleepListItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ListItemSleepTrackerBinding.bind(view)

    fun bind(listItem: UiSleepNight, showSleepDetails: (nightId: Int) -> Unit) {
        binding.listItem.setOnClickListener { showSleepDetails(listItem.nightId) }
        binding.imageSleepQuality.setImageResource(listItem.imageResource)
        binding.textSleepQuality.text = listItem.sleepQuality
    }
}

class SleepNightDiffCallback : DiffUtil.ItemCallback<UiSleepNight>() {

    override fun areItemsTheSame(oldItem: UiSleepNight, newItem: UiSleepNight): Boolean {
        return oldItem.nightId == newItem.nightId
    }

    override fun areContentsTheSame(oldItem: UiSleepNight, newItem: UiSleepNight): Boolean {
        return oldItem == newItem
    }
}
