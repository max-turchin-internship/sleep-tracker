package com.sannedak.sleeptracker.presentation.sleeptracker

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sannedak.sleeptracker.data.repository.SleepRepository
import com.sannedak.sleeptracker.presentation.sleeptracker.model.SleepNightMapper

class SleepTrackerViewModelFactory(
    private val repository: SleepRepository,
    private val mapper: SleepNightMapper
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SleepTrackerViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SleepTrackerViewModel(repository, mapper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
