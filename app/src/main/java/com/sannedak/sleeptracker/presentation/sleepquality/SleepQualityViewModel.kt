package com.sannedak.sleeptracker.presentation.sleepquality

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sannedak.sleeptracker.data.repository.SleepRepository
import com.sannedak.sleeptracker.presentation.UiState
import com.sannedak.sleeptracker.presentation.sleepquality.model.SleepQualityMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SleepQualityViewModel(
    private val nightId: Int,
    private val repository: SleepRepository,
    private val mapper: SleepQualityMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<UiState<Any>>(UiState.Idle)

    val uiState: StateFlow<UiState<Any>>
        get() = _uiState

    fun onSetSleepQuality(sleepQuality: Int) {
        viewModelScope.launch {
            val tonight = repository.getById(nightId) ?: return@launch
            val updatedNight = mapper.mapQuality(tonight, sleepQuality)
            repository.updateInDatabase(updatedNight)
        }
        _uiState.value = UiState.TransitionToAnotherScreen
    }
}
