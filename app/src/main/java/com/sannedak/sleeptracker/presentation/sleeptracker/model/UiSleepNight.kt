package com.sannedak.sleeptracker.presentation.sleeptracker.model

data class UiSleepNight(

    val nightId: Int,
    val sleepQuality: String,
    val imageResource: Int
)
