package com.sannedak.sleeptracker.presentation.sleepdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sannedak.sleeptracker.data.repository.SleepRepository
import com.sannedak.sleeptracker.presentation.UiState
import com.sannedak.sleeptracker.presentation.sleepdetail.model.SleepDetailNightMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SleepDetailViewModel(
    private val nightId: Int,
    private val repository: SleepRepository,
    private val mapper: SleepDetailNightMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<UiState<Any>>(UiState.Idle)

    val uiState: StateFlow<UiState<Any>>
        get() = _uiState

    init {
        loadNight()
    }

    private fun loadNight() {
        viewModelScope.launch {
            val night = repository.getById(nightId) ?: return@launch
            val mappedNight = mapper.mapToUiObject(night)
            _uiState.value = UiState.DetailContent(mappedNight)
        }
    }
}
