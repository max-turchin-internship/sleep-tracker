package com.sannedak.sleeptracker.data.repository

import com.sannedak.sleeptracker.data.model.SleepNight
import com.sannedak.sleeptracker.presentation.UiState
import kotlinx.coroutines.flow.flow

class SleepRepository(private val sleepDao: SleepDao) {

    fun getAllNightsRealtime() = flow {
        emit(UiState.Loading)
        val allNights = sleepDao.getAllNights()
        emit(UiState.FlowFromRepository(allNights))
    }

    suspend fun insertToDatabase(night: SleepNight) {
        sleepDao.insert(night)
    }

    suspend fun updateInDatabase(night: SleepNight) {
        sleepDao.update(night)
    }

    suspend fun getById(id: Int) = sleepDao.getById(id)

    suspend fun clearTableInDatabase() {
        sleepDao.clearTable()
    }

    suspend fun getTonight() = sleepDao.getTonight()
}
