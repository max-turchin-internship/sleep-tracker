package com.sannedak.sleeptracker.data.repository

import androidx.room.*
import com.sannedak.sleeptracker.data.model.SleepNight
import kotlinx.coroutines.flow.Flow

@Dao
interface SleepDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(night: SleepNight)

    @Update
    suspend fun update(night: SleepNight)

    @Query("SELECT * FROM daily_sleep_quality_table WHERE nightId = :id")
    suspend fun getById(id: Int): SleepNight?

    @Query("DELETE FROM daily_sleep_quality_table")
    suspend fun clearTable()

    @Query("SELECT * FROM daily_sleep_quality_table ORDER BY nightId DESC")
    fun getAllNights(): Flow<List<SleepNight>>

    @Query("SELECT * FROM daily_sleep_quality_table ORDER BY nightId DESC LIMIT 1")
    suspend fun getTonight(): SleepNight?
}
