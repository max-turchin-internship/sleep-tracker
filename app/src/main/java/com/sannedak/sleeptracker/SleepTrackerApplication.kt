package com.sannedak.sleeptracker

import android.app.Application
import android.content.res.Resources
import com.sannedak.sleeptracker.data.repository.SleepDatabase
import com.sannedak.sleeptracker.data.repository.SleepRepository
import com.sannedak.sleeptracker.presentation.sleepdetail.model.SleepDetailNightMapper
import com.sannedak.sleeptracker.presentation.sleepquality.model.SleepQualityMapper
import com.sannedak.sleeptracker.presentation.sleeptracker.model.SleepNightMapper

class SleepTrackerApplication : Application() {

    companion object {
        private lateinit var appResources: Resources
    }

    private val database by lazy { SleepDatabase.getDatabase(this) }
    private val _repository by lazy { SleepRepository(database.sleepDao()) }
    private val _uiSleepNightMapper by lazy { SleepNightMapper(appResources) }
    private val _uiSleepQualityMapper by lazy { SleepQualityMapper() }
    private val _uiSleepDetailMapper by lazy { SleepDetailNightMapper(appResources) }

    val repository
        get() = _repository

    val sleepNightMapper
        get() = _uiSleepNightMapper

    val sleepQualityMapper
        get() = _uiSleepQualityMapper

    val sleepDetailMapper
        get() = _uiSleepDetailMapper

    override fun onCreate() {
        super.onCreate()
        appResources = resources
    }
}
